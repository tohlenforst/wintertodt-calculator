<div class="wintertodt-calculator">
    Username: <input type="text" name="username" on:keyup={_ => getStats()} bind:value={settings.username} /><br />
    Goal: <input type="text" name="goal" bind:value={settings.goal} /><br />
    Points per Game: <input type="text" name="points" bind:value={settings.points} /><br />
    Fletching?: <input type="checkbox" name="fletching" bind:checked={settings.fletching} /><br />
    Pyromancer Hood?: <input type="checkbox" name="hood" bind:checked={settings.hood} /><br />
    Pyromancer Garb?: <input type="checkbox" name="garb" bind:checked={settings.garb} /><br />
    Pyromancer Robe?: <input type="checkbox" name="robe" bind:checked={settings.robe} /><br />
    Pyromancer Boots?: <input type="checkbox" name="boots" bind:checked={settings.boots} /><br />
    <button on:click={reset}>Reset</button>
    <hr />
    <div class="error">
        {#if Firemaking}
            {#if settings.goal < Firemaking.LVL}
                {`Goal < Firemaking LVL`}
                <hr />
            {/if}
        {/if}
        {#if settings.points > 13500 || settings.points < 10}
            {`10 <= Points <= 13500`}
            <hr />
        {/if}
        {#if settings.goal > 99 || settings.goal < 51}
            {`51 < Goal < 99`}
            <hr />
        {/if}
    </div>
    {#if Firemaking}
        Rank: {Firemaking.Rank}<br />
        LVL: {Firemaking.LVL}<br />
        XP: {Firemaking.XP}<br />
        Progress: {Firemaking.PercentToNextLVL.toFixed(2)}%<br />
        <progress max="100" value={Firemaking.PercentToNextLVL}></progress><br />
        Total Bonus: {bonus.toFixed(1)}%<br />
        <hr />
    {:else}
        {#if ! settings.username}
            <div class="error">Enter a username</div>
        {:else}
            Loading...
        {/if}
    {/if}
    {#if total}
        Total Kills: {total} {killTime(total)}
        <hr />
    {/if}
    {#if kills.length > 0}
        {#each kills as kill, i}
            {i + Firemaking.LVL + 1}: {kill} {killTime(kill)}<br />
        {/each}
        <hr />
    {/if}
</div>

<script>
    import axios from 'axios'
    import range from 'lodash/range'
    import sum from 'lodash/sum'
    import { getXPFromLvl, xpToNextLvlFrom } from '../tools'

    let settings
    if (localStorage.getItem("settings"))
        settings = JSON.parse(localStorage.getItem("settings"))
    else reset()

    let Firemaking = null
    $: {
        localStorage.setItem("settings", JSON.stringify(settings))
        console.log(JSON.parse(localStorage.getItem("settings")))
    }
    $: bonus = (settings.hood && settings.garb && settings.robe && settings.boots) ? 2.5 : ((settings.hood ? 0.4 : 0) + (settings.garb ? 0.8 : 0) + (settings.robe ? 0.6 : 0) + (settings.boots ? 0.2 : 0))
    $: bonusXP = xp => xp + (xp * (bonus / 100))
    $: logs = Firemaking ? bonusXP((settings.points / 10) * (Firemaking.LVL * 3)) : 0
    $: sticks = Firemaking ? bonusXP((settings.points / 25) * (Firemaking.LVL * 3.8)) : 0
    $: kills = Firemaking ? range(0, settings.goal - Firemaking.LVL)
        .map((kill, i) => {
            if (i == 0) return xpToNextLvlFrom(Firemaking.LVL) - (Firemaking.XP - getXPFromLvl(Firemaking.LVL))
            return xpToNextLvlFrom(Firemaking.LVL + i)
        })
        .map((kill, i) => {
            const xp = (settings.fletching ? sticks : logs) + (Firemaking.LVL * 100)
            return kill / xp
        }) : 0
    $: total = sum(kills)

    let timeout = null
    
    function getStats() {
        clearTimeout(timeout)
        if (settings.username) {
            timeout = setTimeout(() => {
                axios.get("http://rsapi.runomicon.com/player/" + settings.username)
                    .then(res => res.data)
                    .then(player => {
                        Firemaking = player.Stats.Firemaking
                    })
                    .catch(err => err)
            }, 1000)
        }
    }

    function reset() {
        Firemaking = null
        settings = {
            username: "",
            goal: "99",
            points: 500,
            fletching: false,
            hood: false,
            garb: false,
            robe: false,
            boots: false
        }
    }

    function killTime(kill) {
        const s = num => num > 1 ? "s" : "" // determines plurality
        const format = (num, str) => num ? num + " " + str + s(num) : ""
        const avg = 320 // 5 min 20 sec (average kill time)
        const time = kill * avg

        const day = Math.floor(time / 86400)
        const hr = Math.floor((time - (day * 86400)) / 3600)
        const min = Math.floor((time - (day * 86400) - (hr * 3600)) / 60)
        const sec = Math.floor((time - (day * 86400) - (hr * 3600) - (min * 60)))

        const seconds = format(sec, "second")
        const minutes = format(min, "minute") + (min ? " ": "")
        const hours = format(hr, "hour") + (hr ? " ": "")
        const days = format(day, "day") + (day ? " ": "")
        return "(" + days + hours + minutes + seconds + ")"
    }
    getStats()
</script>

<style>
    .wintertodt-calculator {
        margin: 10px;
    }

    .error {
        color: red;
    }

    progress {
        display: inline;
        vertical-align: text-bottom;
    }
</style>