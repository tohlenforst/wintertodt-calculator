import range from 'lodash/range'

export const xpToNextLvlFrom = lvl => 
    Math.round(Math.floor(lvl + 300 * Math.pow(2, lvl / 7)) / 4)

export const getXPFromLvl = lvl =>
    Math.floor(range(1, lvl).reduce((pv, cv) =>
        pv + .25 * Math.floor(cv + 300 * Math.pow(2, cv / 7))
    , 0))

export const getLvlFromXP = (xp, lvl) =>
    lvl ? xp > getXPFromLvl(lvl) ? xp
        : getLvlFromXP(xp, lvl + 1)
    : getLvlFromXP(xp, 1)